CXX = g++
CPPFLAGS = -fpic
RM = rm -rf
BUILD_FOLDER = build
NOISE_FOLDER = build/noise
MKDIR = mkdir -p

## Length of the resulting series
length = 48000

build: build-pink build-white

noise: noise-pink noise-white

build-pink: build-dir-creation pink-noise/voss-algorithm.cpp pink-noise/voss-algorithm.cpp pink-noise/voss-algorithm-burk.cpp pink-noise/trammell-pink-noise.cpp pink-noise/pink-autocorrelated-generator.h pink-noise/pink-autocorrelated-generator.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/pink-voss.o pink-noise/voss-algorithm.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/pink-voss-tweaked.o pink-noise/voss-algorithm-tweaked.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/pink-voss-burk.o pink-noise/voss-algorithm-burk.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/pink-trammell.o pink-noise/trammell-pink-noise.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/pink-autocorrelated-generator.o pink-noise/pink-autocorrelated-generator.cpp

build-dir-creation:
	$(MKDIR) $(BUILD_FOLDER)

noise-dir-creation:
	$(MKDIR) $(NOISE_FOLDER)

noise-pink: noise-dir-creation build-pink
	./$(BUILD_FOLDER)/pink-voss.o $(length) > $(NOISE_FOLDER)/pink-voss.noise
	./$(BUILD_FOLDER)/pink-voss-tweaked.o $(length) > $(NOISE_FOLDER)/pink-voss-tweaked.noise
	./$(BUILD_FOLDER)/pink-voss-burk.o $(length) > $(NOISE_FOLDER)/pink-voss-burk.noise
	./$(BUILD_FOLDER)/pink-trammell.o $(length) > $(NOISE_FOLDER)/pink-trammell.noise
	./$(BUILD_FOLDER)/pink-autocorrelated-generator.o $(length) > $(NOISE_FOLDER)/pink-autocorrelated-generator.noise

build-white: build-dir-creation white-noise/rand-stdlib.cpp white-noise/fast-float-ries.cpp white-noise/white-gaussian-paul.cpp white-noise/white-gaussian-menshikov.cpp white-noise/white-gaussian-smith.cpp white-noise/white-noise-gerd.cpp white-noise/white-gaussian-paul-fixed.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-rand-stdlib.o white-noise/rand-stdlib.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-fast-float-ries.o white-noise/fast-float-ries.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-gaussian-paul.o white-noise/white-gaussian-paul.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-gaussian-menshikov.o white-noise/white-gaussian-menshikov.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-gaussian-smith.o white-noise/white-gaussian-smith.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-noise-gerd.o white-noise/white-noise-gerd.cpp
	$(CXX) $(CPPFLAGS) -o $(BUILD_FOLDER)/white-gaussian-paul-fixed.o white-noise/white-gaussian-paul-fixed.cpp

noise-white: noise-dir-creation build-white
	./$(BUILD_FOLDER)/white-rand-stdlib.o $(length) > $(NOISE_FOLDER)/white-rand-stdlib.noise
	./$(BUILD_FOLDER)/white-fast-float-ries.o $(length) > $(NOISE_FOLDER)/white-fast-float-ries.noise
	./$(BUILD_FOLDER)/white-gaussian-paul.o $(length) > $(NOISE_FOLDER)/white-gaussian-paul.noise
	./$(BUILD_FOLDER)/white-gaussian-menshikov.o $(length) > $(NOISE_FOLDER)/white-gaussian-menshikov.noise
	./$(BUILD_FOLDER)/white-gaussian-smith.o $(length) > $(NOISE_FOLDER)/white-gaussian-smith.noise
	./$(BUILD_FOLDER)/white-noise-gerd.o $(length) > $(NOISE_FOLDER)/white-noise-gerd.noise
	./$(BUILD_FOLDER)/white-gaussian-paul-fixed.o $(length) > $(NOISE_FOLDER)/white-gaussian-paul-fixed.noise

clean:
	$(RM) $(BUILD_FOLDER) $(NOISE_FOLDER)
