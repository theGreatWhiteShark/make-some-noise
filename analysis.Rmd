---
title: "Make some noise!"
subtitle: "Analysis of the quality and performance of different algorithms to produce white and colored noise in C++"
author: "Philipp Mueller (theGreatWhiteShark)"
email: "thetruephil@googlemail.com"
date: "`r format(Sys.time(), '%d %B %Y')`"
output: html_document
---

In [this](https://gitlab.com/theGreatWhiteShark/make-some-noise)
analysis I take a look at the quality and performance of different
algorithms written in C++ producing white or pink noise.

## Motivation and methodology

For a project of mine, namely improving the humanizer of open source
drum computers, I need a fast, high quality, and reliable source of
pink, white, and long correlated noise. Since the literature on
noise-generating algorithm is quite sparse (at least to my knowledge),
I decided to collect various algorithms I found throughout the web and
test them for their performance myself.

### Performance 

The algorithm has to be *fast* because it will be used in real
time. The speed will be accessed using simple time stamps and an
averaged between ten different runs of the algorithm generating the
same number of random points.

### Quality

With quality I am referring to the statistical properties of the
noise. 

**White noise** should have a **flat spectrum**. The flatness will be
accessed by fitting a linear model to the power spectrum of the
generated noise series and reporting its slope. An ideal source would
yield a *slope of 0* for a length going to infinity. As a measure of the
quality of the fit the *variance of the residuals* of the linear model
will be reported as will. The fitting of the linear model will be
performed on the raw power spectrum with no smoothing or
transformation of the axes applied. This is done to be most sensible
to fluctuation in the series.

In addition, some of the algorithms are supposed to create **Gaussian
white noise**. This type has again a flat spectrum and all
realizations are independent of each other. But on top of this the
distribution of the realizations doesn't follow an uniform but a
**Gaussian distribution**. Whether this is actually the case will be
accessed by *fitting a Gaussian probability density function (PDF)
using maximum likelihood* to the series. The comparison between the
empirical first two moments (mean and standard deviation)
and the moments of the fitted Gaussian distribution and the variance
of the residuals between the empirical and fitted PDF will be used as
proxies for the quality of the generated "Gaussian" distribution
function. 

**Pink noise**, on the other hand, is not composed out of independent
realizations. Therefore the power spectrum, the Fourier transform of
the autocovariance according to the [Wiener-Khinchin
theorem](https://en.wikipedia.org/wiki/Wiener%E2%80%93Khinchin_theorem),
has a 1/frequency behavior. Thus it can be fitted by *straight line
with a slope of -1 in a log-log plot*. This will, again, be done using
a linear models and both the slope and the variance of the residuals
will be reported.

Since we are dealing with quasi-random processes in here, the
algorithms are seeded with a random value and all quantities
will be calculated ten-fold and averaged before reporting.

All of these measures will be calculated for various lengths of
generated noise. It's of course quite naive to expect the spectrum of
a short series of let's say 10 points to follow a straight line. But
on the other hand we may just draw a hand-full of realizations and are
not so much interested in the asymptotic behavior of the algorithms.

## Power spectrum of the noise sources

To get a first impression of the quality of the generated noise, let's
have a look at the power spectra of the series.

```{r 'power-spectra',cache=TRUE,echo=TRUE,eval=TRUE,fig.height=12,hightlight=TRUE}
##`
library( ggplot2 ) # Package used to make more appealing plots of the
                                        # spectra 

## Retrieve all compared objects, which will generate the noise.
if ( !dir.exists( "build/" ) ){
  print( "The sources have not been compiled yet. Let's do it now!" )
  system2( "make" )
}

## Find the compiled objects
objects.all <- grep( "\\.o", list.files( "build" ), value = TRUE )
objects.pink <- grep( "pink", objects.all, value = TRUE )
objects.white <- grep( "white", objects.all, value = TRUE )

## Function, which will generate a series from a compiled objects,
## calculates it's power spectrum, and plots/return it using ggplot2.
draw.spectrum <- function( object.vector, title = "" ){
  series.length <- 100000
  series.generated <- lapply( object.vector, function( oo )
    ts( as.numeric( system2( paste0( "./build/", oo ),
                            args = format( series.length,
                                          scientific = FALSE ),
                            stdout = TRUE ) ),
       frequency = 48000 ) )
  
  ## Discard the last element, which contains the time stamp
  series.clean <- lapply( series.generated, function( ss )
    ss[ 1 : ( length( ss ) - 1 ) ] )
  
  ## Calculate the power spectrum of the series
  series.spectra <- lapply( series.clean, function( ss )
    spectrum( ss, plot = FALSE ) )
  
  ## Plot the power spectrum
  series.plot <- data.frame(
      frequency = Reduce( c, lapply( series.spectra, function( ss )
        ss$freq ) ),
      spectrum = Reduce( c, lapply( series.spectra, function( ss )
        ss$spec ) ),
      algorithm =
        Reduce( c, lapply( seq( 1, length( object.vector ) ),
                          function( ii ) 
                            rep( object.vector[ ii ],
                                length( series.spectra[[ ii ]]$freq )
                                ) ) ) )

  ggplot( data = series.plot, aes( x = frequency, y = spectrum ) ) +
    geom_line( color = "navy" ) +
    geom_smooth( color = "darkorange", fill = "darkorange",
                method = "gam", alpha = .3,
                formula = y ~ s( x, bs = 'cs' ) ) +
    facet_grid( algorithm ~ ., scales = "free" ) +
    theme_bw() + scale_y_log10() + scale_x_log10() +
    theme( strip.background = element_rect( fill =  "sienna1" ) ) +
    ggtitle( title )
  
  return( last_plot() )
}

draw.spectrum( objects.white,
              title = "Power spectra of the white noise sources" )
draw.spectrum( objects.pink,
              title = "Power spectra of the pink noise sources" )

```

By visual inspection all of the **white noise** sources perform
reasonable well and it is not possible to pick a the best performing
algorithm by eye.

For the **pink noise**, on the other hand, only the
[pink-autocorrelated-generator.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/pink-noise/pink-autocorrelated-generator.cpp)
and
[pink-voss-burk.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/pink-noise/voss-algorithm-burk.cpp)
algorithm do yield a reasonably flat power spectrum with a linear
slope in the log-log plot.

## Performance of the white noise sources

```{r 'white-noise-performance', dependson='power-spectra',cache=TRUE,echo=TRUE,eval=TRUE,fig.height=7}

library( stats4 ) # To perform maximum likelihood fits. 

performance.test.white <- function( object, 
                                   series.lengths =
                                     c( 10, 100, 200, 500, 1000, 2000,
                                       5000, 10000, 20000, 50000,
                                       100000, 200000 ) ){
  ## Object the result will be stored in
  series.summary <- data.frame(
      length = rep( NA, length( series.lengths ) ),
      duration = rep( NA, length( series.lengths ) ),
      linear.slope = rep( NA, length( series.lengths ) ),
      linear.residual.variance = rep( NA, length( series.lengths ) ),
      mean = rep( NA, length( series.lengths ) ),
      sd = rep( NA, length( series.lengths ) ),
      gaussian.mean = rep( NA, length( series.lengths ) ),
      gaussian.sd = rep( NA, length( series.lengths ) ),
      gaussian.residual.variance = rep( NA, length( series.lengths ) ) )
  
  ## Perform the test for all lengths
  for ( ll in 1 : length( series.lengths ) ){
    length <- series.lengths[ ll ]
    
    ## Draw number.of.draws series of the corresponding length and
    ## average the time needed by the algorithm.
    number.of.draws <- 10
    duration.sum <- linear.slope <- residual.variance <- 
      series.mean <- series.sd <- gaussian.mean <- 
      gaussian.sd <- gaussian.residual.variance <- 0

    for ( nn in 1 : number.of.draws ){
      series.raw <- ts( as.numeric(
          system2( paste0( "./build/", object ),
                  args = format( length, scientific = FALSE ),
                  stdout = TRUE ) ), frequency = 48000 )
      ## The last element of the series is used by the underlying
      ## algorithm to store the time passed during the generation of
      ## the random series.
      duration.sum <- duration.sum +
        as.numeric( series.raw[ length( series.raw ) ] )


      ## Remove the time stamp
      series <- series.raw[ 1 : ( length( series.raw ) - 1 ) ]

      ## Power spectrum of the series
      series.spectrum <- spectrum( series, plot = FALSE )

      ## Fit linear models into the spectrum. 
      series.plot <- data.frame(
          spectrum = series.spectrum$spec,
          frequency = series.spectrum$freq )
      
      series.model.linear <- lm( spectrum ~ frequency,
                                data = series.plot )
      ## The variance of the residuals will be used as a proxy for the
      ## quality of the fit.

      ## Fit the data to an Gaussian distribution via the method of
      ## maximum likelihood.
      gaussian.neg.log.likelihood <- function( mean, sd ){
        likelihood <- suppressWarnings( dnorm( series, mean, sd ) )
        return( -sum( log( likelihood ) ) )
      }
      
      series.gaussian <- mle(
          minuslogl = gaussian.neg.log.likelihood,
          start = list( mean = 0, sd = 1 ) )

      ## Calculate the variance of the residuals between the empirical
      ## probability density distribution and the fitted Gaussian one.
      series.density <- density( series )

      series.density.residuals <- series.density$y -
        dnorm( series.density$x, mean = series.gaussian@coef[ 1 ],
              sd = series.gaussian@coef[ 2 ] )

      ## Wrap up and summing of the results
      linear.slope <- linear.slope +
        as.numeric( series.model.linear$coefficients[ 2 ] )
      residual.variance <- residual.variance +
        var( series.model.linear$residuals )
      series.mean <- series.mean + mean( series ) 
      series.sd <- series.sd + sd( series )
      gaussian.mean <- gaussian.mean +
        as.numeric( series.gaussian@coef[ 1 ] )
      gaussian.sd <- gaussian.sd +
        as.numeric( series.gaussian@coef[ 2 ] )
      gaussian.residual.variance <- gaussian.residual.variance +
        var( series.density.residuals )
    }

    series.summary[ ll,  ] <- c(
        length, duration.sum/ number.of.draws,
        linear.slope/ number.of.draws,
        residual.variance/ number.of.draws,
        series.mean/ number.of.draws,
        series.sd/ number.of.draws,
        gaussian.mean/ number.of.draws,
        gaussian.sd/ number.of.draws,
        gaussian.residual.variance/ number.of.draws )
  }

  return( series.summary )
}

series.lengths <- c( 10, 100, 200, 500, 1000, 2000, 5000, 10000,
                    20000, 50000, 100000, 200000 )
summary.white <- lapply( objects.white, function( oo )
  performance.test.white( oo, series.lengths ) )

plot.duration <- data.frame(
    length = Reduce( c, lapply( summary.white, function( oo )
      oo$length ) ),
    duration = Reduce( c, lapply( summary.white, function( oo )
      oo$duration ) ),
    algorithm = Reduce( c, lapply( objects.white, function( cc )
      rep( cc, 12 ) ) ) )

ggplot( data = plot.duration, aes( x = length, y = duration,
                                  color = algorithm ) ) +
  geom_line( ) + geom_point() + scale_x_log10() + scale_y_log10() +
  scale_color_brewer( palette = "Set2" ) +
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ylab( "generation time in seconds" ) +
  ggtitle( "Performance of the algorithmn in terms of speed" ) +
  theme_bw()

## Check the flatness of the spectrum
plot.linear.model <- data.frame(
    length =  Reduce( c, lapply( summary.white, function( oo )
      oo$length ) ),
    value = c( Reduce( c, lapply( summary.white, function( oo )
      oo$linear.slope ) ),
      Reduce( c, lapply( summary.white, function( oo )
        oo$linear.residual.variance ) ) ),
    parameter = c( rep( "slope - linear model",
                       length( series.lengths ) *
                       length( summary.white ) ),
                  rep( "residual variance - linear model",
                      length( series.lengths ) *
                      length( summary.white ) ) ),
    algorithm = rep(
        Reduce( c, lapply( objects.white, function( cc )
          rep( cc, length( series.lengths ) ) ) ), 2 ) )

ggplot( data = plot.linear.model,
       aes( x = length, y = value, color = algorithm ) ) +
  geom_line() + geom_point() + scale_x_log10() + 
  facet_grid( parameter ~ ., scales = "free" ) + theme_bw() + 
  scale_color_brewer( palette = "Set2" ) +
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ggtitle( "Flatness of the power spectra" )

## Check for having a Gaussian distribution in the noise
summary.gaussian <- summary.white[  
    grep( "gaussian", objects.white ) ]

plot.gaussian.model <- data.frame(
    length = Reduce( c, lapply( summary.gaussian, function( oo )
      oo$length ) ),
    value = c( Reduce( c, lapply( summary.gaussian, function( oo )
      oo$mean ) ),
      Reduce( c, lapply( summary.gaussian, function( oo )
        oo$sd ) ),
      Reduce( c, lapply( summary.gaussian, function( oo )
        oo$gaussian.mean ) ),
      Reduce( c, lapply( summary.gaussian, function( oo )
        oo$gaussian.sd ) ) ),
    parameter = c( rep( "mean",
                       length( series.lengths ) *
                       length( summary.gaussian ) ),
                  rep( "standard deviation",
                      length( series.lengths ) *
                      length( summary.gaussian ) ),
                  rep( "mean",
                      length( series.lengths ) *
                      length( summary.gaussian ) ),
                  rep( "standard deviation",
                      length( series.lengths ) *
                      length( summary.gaussian ) ) ),
    source = c( rep( "raw",
                    length( series.lengths ) *
                    length( summary.gaussian ) ),
               rep( "raw",
                   length( series.lengths ) *
                   length( summary.gaussian ) ),
               rep( "model",
                   length( series.lengths ) *
                   length( summary.gaussian ) ),
               rep( "model",
                   length( series.lengths ) *
                   length( summary.gaussian ) ) ),
    algorithm = rep(
        Reduce( c, lapply( grep( "gaussian", objects.white,
                                value = TRUE ), function( cc )
                                  rep( cc, 12 ) ) ), 4 ) )

ggplot( data = plot.gaussian.model,
       aes( x = length, y = value, color = algorithm,
           shape = source, linetype = source ) ) +
  geom_line() + geom_point() + scale_x_log10() +
  theme_bw() + facet_grid( parameter ~ ., scales = "free" ) +
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ggtitle( "Empirial moments vs. Gaussian model" )


## Plotting the negative log-likelihood of the Gaussian fits
plot.gaussian.likelihood <- data.frame(
    length = Reduce( c, lapply( summary.gaussian, function( oo )
      oo$length ) ),
    value = Reduce( c, lapply( summary.gaussian, function( oo )
      oo$gaussian.residual.variance ) ),
    algorithm = 
      Reduce( c, lapply( grep( "gaussian", objects.white,
                              value = TRUE ), function( cc )
                                rep( cc, 12 ) ) ) )

ggplot( data = plot.gaussian.likelihood,
       aes( x = length, y = value, color = algorithm ) ) +
  geom_line() + geom_point() + scale_x_log10() + scale_y_log10() +
  theme_bw() + 
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ggtitle( "The residual variance of the fitted Gaussian densities" )


```

### Speed of the algorithms

All of the noise sources have both similar performance and
scaling. There is no algorithm, which is constantly outperforming the
others. Therefore all of them can be more or less treated equally in
terms of speed.

### Flatness of the spectra

We are looking for a routine generating noise both a slope and
variance of the residuals of approximately zero regardless of the
length of the generated series.

The **rand()** function from the C++ standard library outperforms all
other algorithms in terms of the flatness and the quality of the
residuals. It is only matched by the
[white-fast-float-ries.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/fast-float-ries.cpp)
algorithm having a similar variance in the residuals and a slightly
bigger but comparable slope in the power spectrum.

Regarding the **Gaussian white noise** sources the 
[white-gaussian-paul.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/white-gaussian-paul.cpp)
is the best one performing as good as the uniform white noise
sources. The reason is because it is actually badly implemented. It
tries to use the central limit theorem by adding different noise
sources in order to produce white Gaussian noise. But since it adds
one and the same noise source, the distribution is not Gaussian at
all (as can be seen in the variance of the residuals of the fitted
PDF). For the **true Gaussian white noise** sources the 
[white-gaussian-paul-fixed.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/white-gaussian-paul-fixed.cpp)
is performs slightly better.

### Approximation of the Gaussian PDF

The comparison of the moments show that all the true Gaussian noise
sources have zero mean and unit variance. For small lengths the fixed
version of Paul's and Smith's algorithm are yielding the best
result. Judging from the variance of the resulting residuals the
[white-gaussian-smith.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/white-gaussian-smith.cpp) is
the one of highest quality.

## Performance of the pink noise sources

```{r 'pink-noise-performance', dependson='power-spectra',cache=TRUE,echo=TRUE,eval=TRUE,fig.height=7}

library( stats4 ) # To perform maximum likelihood fits. 

performance.test.pink <- function( object, 
                                   series.lengths =
                                     c( 10, 100, 200, 500, 1000, 2000,
                                       5000, 10000, 20000, 50000,
                                       100000, 200000 ) ){
  ## Object the result will be stored in
  series.summary <- data.frame(
      length = rep( NA, length( series.lengths ) ),
      duration = rep( NA, length( series.lengths ) ),
      linear.slope = rep( NA, length( series.lengths ) ),
      linear.residual.variance = rep( NA, length( series.lengths ) ),
      mean = rep( NA, length( series.lengths ) ),
      variance = rep( NA, length( series.lengths ) ) )
  
  ## Perform the test for all lengths
  for ( ll in 1 : length( series.lengths ) ){
    length <- series.lengths[ ll ]
    
    ## Draw number.of.draws series of the corresponding length and
    ## average the time needed by the algorithm.
    number.of.draws <- 10
    duration.sum <- linear.slope <- residual.variance <- 
      series.mean <- series.variance <- 0

    for ( nn in 1 : number.of.draws ){
      series.raw <- ts( as.numeric(
          system2( paste0( "./build/", object ),
                  args = format( length, scientific = FALSE ),
                  stdout = TRUE ) ), frequency = 48000 )
      ## The last element of the series is used by the underlying
      ## algorithm to store the time passed during the generation of
      ## the random series.
      duration.sum <- duration.sum +
        as.numeric( series.raw[ length( series.raw ) ] )


      ## Remove the time stamp
      series <- series.raw[ 1 : ( length( series.raw ) - 1 ) ]

      ## Power spectrum of the series
      series.spectrum <- spectrum( series, plot = FALSE )

      ## Fit linear models into the spectrum. 
      series.plot <- data.frame(
          spectrum = log( series.spectrum$spec ),
          frequency = log( series.spectrum$freq ) )
      
      series.model.linear <- lm( spectrum ~ frequency,
                                data = series.plot )
      ## The variance of the residuals will be used as a proxy for the
      ## quality of the fit.

      ## Wrap up and summing of the results
      linear.slope <- linear.slope +
        as.numeric( series.model.linear$coefficients[ 2 ] )
      residual.variance <- residual.variance +
        var( series.model.linear$residuals )
      series.mean <- series.mean + mean( series ) 
      series.variance <- series.variance + var( series )
    }

    series.summary[ ll,  ] <- c(
        length, duration.sum/ number.of.draws,
        linear.slope/ number.of.draws,
        residual.variance/ number.of.draws,
        series.mean/ number.of.draws,
        series.variance/ number.of.draws )
  }

  return( series.summary )
}

series.lengths <- c( 10, 100, 200, 500, 1000, 2000, 5000, 10000,
                    20000, 50000, 100000, 200000 )
summary.pink <- lapply( objects.pink, function( oo )
  performance.test.pink( oo, series.lengths ) )

plot.duration <- data.frame(
    length = Reduce( c, lapply( summary.pink, function( oo )
      oo$length ) ),
    duration = Reduce( c, lapply( summary.pink, function( oo )
      oo$duration ) ),
    algorithm = Reduce( c, lapply( objects.pink, function( cc )
      rep( cc, 12 ) ) ) )

ggplot( data = plot.duration, aes( x = length, y = duration,
                                  color = algorithm ) ) +
  geom_line( ) + geom_point() + scale_x_log10() + scale_y_log10() +
  scale_color_brewer( palette = "Set2" ) +
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ylab( "generation time in seconds" ) +
  ggtitle( "Performance of the algorithmn in terms of speed" ) +
  theme_bw()

## Check the flatness of the spectrum
plot.linear.model <- data.frame(
    length =  Reduce( c, lapply( summary.pink, function( oo )
      oo$length ) ),
    value = c( Reduce( c, lapply( summary.pink, function( oo )
      oo$linear.slope ) ),
      Reduce( c, lapply( summary.pink, function( oo )
        oo$linear.residual.variance ) ) ),
    parameter = c( rep( "slope - linear model",
                       length( series.lengths ) *
                       length( summary.pink ) ),
                  rep( "residual variance - linear model",
                      length( series.lengths ) *
                      length( summary.pink ) ) ),
    algorithm = rep(
        Reduce( c, lapply( objects.pink, function( cc )
          rep( cc, length( series.lengths ) ) ) ), 2 ) )

ggplot( data = plot.linear.model,
       aes( x = length, y = value, color = algorithm ) ) +
  geom_line() + geom_point() + scale_x_log10() +
  facet_grid( parameter ~ ., scales = "free" ) + theme_bw() + 
  scale_color_brewer( palette = "Set2" ) +
  theme( strip.background = element_rect( fill =  "sienna1" ) ) +
  ggtitle( "Decrease of the power spectra" )

```

### Speed of the algorithms

As for the white noise, there is no pink noise algorithm which can be
considered the consistently best performing one.

### Quality of the power spectrum

Since there is no mayor difference in the speed the algorithm
producing the best 1/frequency power spectrum will be considered the
best one. It thus has to have the least variance of the residuals and
a slope of approximately -1.

In terms of the *variance of the residuals* the performance of the
algorithms does look pretty similar. 

Considering the *slope of the power spectrum* both the
[pink-voss-burk.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/pink-noise/voss-algorithm-burk.cpp) and
[pink-autocorrelated-generator.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/pink-noise/pink-autocorrelated-generator.cpp)
produce the best results.

Since the voss-burk algorithm seems to be slightly faster, it will be
the one of choice in my following projects whenever it's time to
generate some pink noise.

## Conclusion

After inspecting the performance and quality of several different
algorithms generating white and pink noise I picked the following as
the best performing ones.

- **uniform white noise**: **std::rand()**
- **Gaussian white noise**: [white-gaussian-smith.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/white-gaussian-smith.cpp)
- **pink noise**: [voss-algorithm-burk.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/pink-noise/voss-algorithm-burk.cpp)

E.g. considering the uniform white noise sources the
[fast-float-ries.cpp](https://gitlab.com/theGreatWhiteShark/make-some-noise/blob/master/white-noise/fast-float-ries.cpp) algorithm is
faster then the implementation of the **rand** function in the
**stdlib** of C++. But the latter produces noise of higher
quality. And since the performance of the algorithms in speed is
comparable, I would trade the slow down against improved quality.
