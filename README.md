In this project I explore different algorithms generating pink and
white noise in C++.

The analysis can be accessed using [GitLab
pages](https://thegreatwhiteshark.gitlab.io/make-some-noise/analysis.html).

## Motivation

For another project of mine I do need a reliable (both efficient and
high-quality) source of pink noise written in C++. 

In order to find a nice algorithm, I will explore a lot of **C++ code**
and suggestions I found in the web and analyse the resulting noise in
the statistical programming language **R**. While I'm at it I will
also compile a list of useful references and have a look at other
colors of noise.

## Results

After inspecting the performance and quality of several different
algorithms generating white and pink noise I picked the following as
the best performing ones.

- **uniform white noise**: **std::rand()**
- **Gaussian white noise**: [white-gaussian-smith.cpp](white-noise/white-gaussian-smith.cpp)
- **pink noise**: [voss-algorithm-burk.cpp](pink-noise/voss-algorithm-burk.cpp)

## Usage

To compile all the C++ code, just enter the
following command in the package root.

``` bash
make
```

To also create the noise from the different algorithms, use

``` bash
make noise length='1000'
```

with the string after `length` specifying the number of random
floats created by the algorithm.

A proper analysis of the noise algorithms is given in
[analysis.Rmd](analysis.Rmd). To reproduce it at home, use the
following commands in a R shell.

``` R
## Make sure all package requirements are fulfilled.
library( ggplot2 )
library( rmarkdown )

## Evaluate all chunks containing R code and compiling a HTML file,
## which will contain the results of the analysis. It can be opened
## using your default browser.
render( "analysis.Rmd" )
```

