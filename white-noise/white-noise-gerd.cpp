/* 
   Posted by Gerd on the music.dsp mailing list
   http://www.musicdsp.org/showone.php?id=216

   Author: Philipp Müller, thetruephil@googlemail.com
*/

#include <iostream>
#include <ctime>
#include <cstdlib>

#define DEFAULT_LENGTH 48000* 4

using namespace std;

float g_fScale = 2.0f / 0xffffffff;
int g_x1 = 0x67452301;
int g_x2 = 0xefcdab89;

void whitenoise(
  float* _fpDstBuffer, // Pointer to buffer
  unsigned int _uiBufferSize, // Size of buffer
  float _fLevel ) // Noiselevel (0.0 ... 1.0)
{
  _fLevel *= g_fScale;

  while( _uiBufferSize-- )
  {
    g_x1 ^= g_x2;
    *_fpDstBuffer++ = g_x2 * _fLevel;
    cout << g_x2 * _fLevel << endl;
    g_x2 += g_x1;
  }
}

int main( int argc, char* argv[] ){
  int seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH/ 4;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 )/ 4;
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  float* buffer = new float[ seriesLength* 4 ];
  whitenoise( buffer, sizeof(float)*seriesLength, 1.0 );
  delete[] buffer;
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
