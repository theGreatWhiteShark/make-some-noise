/*
  Posted by Alexey Menshikov on the music.dsp mailing list 
  http://www.musicdsp.org/showone.php?id=109

  Author: Philipp Müller, thetruephil@googlemail.com
*/

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>

#define DEFAULT_LENGTH 48000

using namespace std;

#define ranf() ((float) rand() / (float) RAND_MAX)

float ranfGauss (int m, float s)
{
   static int pass = 0;
   static float y2;
   float x1, x2, w, y1;

   if (pass)
   {
      y1 = y2;
   } else  {
      do {
         x1 = 2.0f * ranf () - 1.0f;
         x2 = 2.0f * ranf () - 1.0f;
         w = x1 * x1 + x2 * x2;
      } while (w >= 1.0f);

      w = (float)sqrt (-2.0 * log(w) / w);
      y1 = x1 * w;
      y2 = x2 * w;
   }
   pass = !pass;

   return ( (y1 * s + (float) m));
}

int main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  // Seeding the random number generator
  std::srand( std::time( 0 ) );
  
  int offset = 0;
  float scale = 1;
  
  for ( int ii = 0; ii < seriesLength; ii++ ){
    cout << ranfGauss( offset, scale ) << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
