/* 
   A port of the delphi noise generator by tobybears to C++ by Paul.
   http://www.musicdsp.org/showone.php?id=168

   Modifications: 
     - In order to work properly and use the central limit
       theorem, DIFFERENT sources of noise will be added.
     - The objective is to produce Gaussian white noise of zero mean
       and unit variance. In order to achieve the latter the sum of
       the variance of the individual noise sources must be equal to
       one. Since the variance of uniform noise between 0 and 1 is
       1/12, 12 individual sources are used to produce the Gaussian
       noise signal.

   Author: Philipp Müller, thetruephil@googlemail.com
   Copyleft. No rights reserved.
*/

/* Include requisites */
#include <cstdlib>
#include <ctime>
#include <iostream>

#define DEFAULT_LENGTH 48000

using namespace std;


/* Setup constants */
const static int q = 15; // Precision of the generated noise.
const static float c1 = (1 << q) - 1;
const static float c2 = ((int)(c1)) + 1; // Maximum value
const static float c3 = 1.f / c1; // Normalization term.


int main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  /* Generate a new random seed from system time - do this once in your constructor */
  std::srand( std::time(0) );
  /* random number in range 0 - 1 not including 1 */
  float random = 0.f;

  // Sum 12 sources of uniform white noise. Since every single source
  // has got a variance of 1/12, the resulting variance of the
  // Gaussian white noise will be 1.
  int numberOfNoisesSummed = 12;
  
  /* the white noise */
  float noise = 0.f;

  for ( int ii = 0; ii < seriesLength; ii++){
    // This term is needed to center the random series around 0.
    noise = - ((float)numberOfNoisesSummed/2) * ( c2 - 1.f ) * c3;
  
    for ( int rr = 0; rr < numberOfNoisesSummed; rr++ ){
      noise += ((float)rand()/RAND_MAX) * c2 * c3;
    }
    cout << noise << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
