/*
  Taken from the musicdsp source code archive.

  Original author/posted by: Dominik Ries
  Author: Philipp Müller, thetruephil@googlemail.com 
  Copyleft. No rights reserved.

  Notes :
  a small and fast implementation for random float numbers in the
  range [-1,1], usable as white noise oscillator. 

  compared to the naive usage of the rand() function it gives a
  speedup factor of 9-10.

  compared to a direct implementation of the rand() function (visual
  studio implementation) it still gives a speedup by a factor of 2-3. 

  apart from beeing faster it also provides more precision for the
  resulting floats since its base values use full 32bit precision. 
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

#define DEFAULT_LENGTH 48000

using namespace std;

// set the initial seed to whatever you like
static int randSeed = 0;

// fast rand float, using full 32bit precision
// takes about 12 seconds for 2 billion calls

// The function was rescaled to produce numbers between 0 and 1.
float Fast_RandFloat(){
  randSeed *= 16807;
  return (float)randSeed * 4.6566129e-010f/2 + 0.5;
}

int main( int argc, char* argv[] ){
  float seriesLength;

  std::srand( std::time(0) );
  randSeed = rand();

  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  float noise;

  for ( int ii = 0; ii < seriesLength; ii++ ){
    noise = Fast_RandFloat();
    cout << noise << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
