/* 
   This script will use the default std::rand() function to produce 
   random numbers. It will serve as a reference.

   Author: Philipp Müller, thetruephil@googlemail.com
   Copyleft. No rights reserved.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

#define DEFAULT_LENGTH 48000

using namespace std;

int main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  // Seeding the random number generator
  std::srand( std::time( 0 ) );
  
  for ( int ii = 1; ii < seriesLength; ii++ ){
    cout << (float)rand()/RAND_MAX << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
