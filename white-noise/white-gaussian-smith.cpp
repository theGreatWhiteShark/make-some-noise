/* 
   Posted by Remage at the music.dsp mailing list
   http://www.musicdsp.org/showone.php?id=113

   SOURCE:

   Steven W. Smith:
   The Scientist and Engineer's Guide to Digital Signal Processing

  Author: Philipp Müller, thetruephil@googlemail.com
*/

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

#define DEFAULT_LENGTH 48000

#define PI 3.1415926536f

using namespace std;

float RandNumber(){
  float R1 = (float) rand() / (float) RAND_MAX;
  float R2 = (float) rand() / (float) RAND_MAX;

  return (float) sqrt( -2.0f * log( R1 )) * cos( 2.0f * PI * R2 );
}

int main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  // Seeding the random number generator
  std::srand( std::time( 0 ) );
  
  for ( int ii = 0; ii < seriesLength; ii++ ){
    cout << RandNumber() << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
      
