/* 
   A port of the delphi noise generator by tobybears to C++ by Paul.
   http://www.musicdsp.org/showone.php?id=168

   Notes: since only one realization of a random processes is used,
   the algorithm fails to use the central limit theorem to produce
   Gaussian white noise. Check out the white-gaussian-paul-fixed.cpp
   algorithm for a corrected version.

   Author: Philipp Müller, thetruephil@googlemail.com
   Copyleft. No rights reserved.
*/

/* Include requisits */
#include <cstdlib>
#include <ctime>
#include <iostream>

#define DEFAULT_LENGTH 48000

using namespace std;


/* Setup constants */
const static int q = 15; // Precision of the generated noise.
const static float c1 = (1 << q) - 1;
const static float c2 = ((int)(c1 / 3)) + 1;
const static float c3 = 1.f / c1;


int main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  /* Generate a new random seed from system time - do this once in your constructor */
  std::srand( std::time(0) );
  /* random number in range 0 - 1 not including 1 */
  float random = 0.f;

  /* the white noise */
  float noise = 0.f;

  for (int i = 0; i < seriesLength; i++)
    {
      random = ((float)rand() / (float)(RAND_MAX));
      noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
      cout << noise << endl;
    }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
