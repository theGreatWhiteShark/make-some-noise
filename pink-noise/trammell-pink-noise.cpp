/*
  http://www.musicdsp.org/showone.php?id=220

  Author: Philipp Müller, thetruephil@googlemail.com
*/


#ifndef _PinkNoise_H
#define _PinkNoise_H

// Technique by Larry "RidgeRat" Trammell 3/2006
// http://home.earthlink.net/~ltrammell/tech/pinkalg.htm
// implementation and optimization by David Lowenfels

#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

#define PINK_NOISE_NUM_STAGES 3

#define DEFAULT_LENGTH 48000

class PinkNoise {
public:
  PinkNoise() {
    clear();
  }

  void clear() {
    for( size_t i=0; i< PINK_NOISE_NUM_STAGES; i++ )
      state[ i ] = 0.0;
    }
    
  float tick() {
    static const float RMI2 = 2.0 / float(RAND_MAX); // + 1.0; // change for range [0,1)
    static const float offset = A[0] + A[1] + A[2];

  // unrolled loop
    float temp = float( rand() );
    state[0] = P[0] * (state[0] - temp) + temp;
    temp = float( rand() );
    state[1] = P[1] * (state[1] - temp) + temp;
    temp = float( rand() );        
    state[2] = P[2] * (state[2] - temp) + temp;
    return ( A[0]*state[0] + A[1]*state[1] + A[2]*state[2] )*RMI2 - offset;
  }

protected:
  float state[ PINK_NOISE_NUM_STAGES ];
  static const float A[ PINK_NOISE_NUM_STAGES ];
  static const float P[ PINK_NOISE_NUM_STAGES ];
};

const float PinkNoise::A[] = { 0.02109238, 0.07113478, 0.68873558 }; // rescaled by (1+P)/(1-P)
const float PinkNoise::P[] = { 0.3190,  0.7756,  0.9613  };

#endif

int main( int argc, char* argv[] ){

  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  // Seeding the random number generator
  std::srand( std::time( 0 ) );

  PinkNoise noise;

  for ( int ii = 0; ii < seriesLength; ii++ ){
    std::cout << noise.tick() << std::endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
