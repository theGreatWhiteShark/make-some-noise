// Taken from http://www.firstpr.com.au/dsp/pink-noise/

/* 
This algorithms generates five independent sources of white noise,
sums their results, and updates them on different time scales (with
logarithmic distances in time).
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

#define DEFAULT_LENGTH 48000

using namespace std;

class PinkNumber {
private:
  int max_key;
  int key;
  unsigned int white_values[ 5 ];
  unsigned int range;

public:
  PinkNumber( unsigned int range = 128 ){
    max_key = 0x1f; // Five bits set // 32
    this->range = range;
    key = 0;
    for ( int ii = 0; ii < 5; ii++ ){
      white_values[ ii ] = rand() % ( range/ 5 );
    }
  }

  int GetNextValue(){
    int last_key = key;
    unsigned int sum;


    key++;
    if ( key > max_key ){
      key = 0;
    }

    // Exclusive-Or previous value with current value. This gives a
    // list of bits that have changed.
    // For every second event the difference will be 00000001, for
    // every fourth 00000011, for every eight 00000111 and so on...
    int diff = last_key ^ key;
    sum = 0;
    for ( int ii = 0; ii < 5; ii++ ){
      // If a bit changed, get a new random number for corresponding
      // white_value
      // The shift operator at the right-hand side of the bitwise AND
      // operator determines the number of updates of the noise source
      // by the position of the leading 1 in `diff`. For 00000001
      // there will be one update, for 00000111 there will be three.
      if ( diff & ( 1 << ii ) ){
	white_values[ ii ] = rand() % ( range/ 5 );
      }
      sum += white_values[ ii ];
    }
    return sum;
  }
};

main( int argc, char* argv[] ){
  float seriesLength;
  
  // If a command line input was supplied, this number will be used as
  // the length of the resulting series
  if ( argc < 2 ){
    seriesLength = DEFAULT_LENGTH;
  } else {
    seriesLength = strtol( argv[ 1 ], &argv[1], 0 );
  }
  
  // Time stamp to determine the speed of the algorithm
  const clock_t generationStart = clock();

  // Seeding the random number generator
  std::srand( std::time( 0 ) );

  PinkNumber pn;

  for ( int ii = 0; ii < seriesLength; ii++ ){
    cout << ((float) pn.GetNextValue()/ 128 ) << endl;
  }
  cout << float( clock() - generationStart ) / CLOCKS_PER_SEC << endl;
}
